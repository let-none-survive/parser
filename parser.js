'use strict';
const request = require('request');
const cheerio = require("cheerio");
const xl = require('excel4node');
const wb = new xl.Workbook();
const ws = wb.addWorksheet('Sheet 1');
let colNames = ['', '  TITLE  ', ' PRICE'];
let nextPageUrl;

let url = (arr) => {
  let mainReqProm = new Promise((resolve, reject) => {
    request(arr, (error, response, body) => {
      resolve(body);
    });
  });
  for (let x = 1; x < 3; x++) {
    ws.cell(1, x).string(colNames[x]);
  }

  mainReqProm.then((body) => {
    let $ = cheerio.load(body);
    let itemRequests = [];
    let clearStr = (str) => {
      str.replace(/(\r\n|\n|\r)/gm, "");
      return str;
    }
    $('.lvtitle').find('a').each((i, val) => {

      let itemUrl = $(val).attr('href');

      let itemReqProm = new Promise((resolve) => {

        request(itemUrl, (error, response, body) => {
          ws.lastUsedRow++;
          let page = cheerio.load(body);
          page('.g-hdn').remove();

          ws.cell(ws.lastUsedRow, 1).string(clearStr(page('#itemTitle').text()));
          ws.cell(ws.lastUsedRow, 2).string(clearStr(page('#prcIsum').text()));

          resolve(body)
        });
      });

      itemRequests.push(itemReqProm);
    });
    if ($('.pg.curr').next().attr('href') !== undefined && $('.pg.curr').next().attr('href').length > 0 && $('a.gspr.next-d').length == 0) {
      nextPageUrl = $('.pg.curr').next().attr('href');
      url(nextPageUrl);
    }

    return Promise.all(itemRequests);

  }).then(() => {
    console.log('Страница загружена');
    wb.write('parser.xlsx');
  });
};
url('https://www.ebay.com/sch/Computers-Tablets-Networking-/58058/i.html?_nkw=Apple');
